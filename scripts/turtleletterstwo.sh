#!/usr/bin/bash

rosservice call /reset
rosservice call /turtle1/set_pen 100 0 0 2 1
rosservice call /turtle1/teleport_absolute 1 1 0
rosservice call /turtle1/set_pen 100 0 0 2 0

./turtleletter.sh

rosservice call /spawn 4 1 0  turtle2
rosservice call /turtle2/set_pen 0 200 50 2 0

rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
         -- '[0.0, 4.0, 0.0]' '[0.0, 0.0, 0.0]'

rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
         -- '[2.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'

rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
         -- '[-2.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'

rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
         -- '[0.0, -2.0, 0.0]' '[0.0, 0.0, 0.0]'

rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
         -- '[2.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'

rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
         -- '[-2.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'

rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
         -- '[0.0, -2.0, 0.0]' '[0.0, 0.0, 0.0]'

rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
         -- '[2.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'


