# Kent Yamamoto
# ME 555.11 - Midterm Project Code
# Drawing initials K, E, Y (my actual initials are KKY) using EE of UR5 robot

# import all packages required #
import sys
import copy
import rospy
import time
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from math import pi
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

# initialize Moveit and start communication with ROS
moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('move_group_python_interface_tutorial',
                anonymous=True)

# initialize robot and scene
robot = moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()

# specify that I'm referring to the manipulator position and orientation from the uR5 URDF file
group_name = "manipulator"
move_group = moveit_commander.MoveGroupCommander(group_name)

# initialize joint_gla variable by collecting starting position
joint_goal = move_group.get_current_joint_values()

## DEFINE SOME RECURRING FUNCTIONS ##

# function to come back to defined "home config"
def home_config():
    # specify joint_goal values for starting "home" position
    joint_goal[0] = -2.211
    joint_goal[1] = -0.955
    joint_goal[2] = -2.211
    joint_goal[3] = -0.021
    joint_goal[4] = 0.999
    joint_goal[5] = 1.610

    # function to move EE to desired position and orientation
    move_group.go(joint_goal, wait=True)
    move_group.stop()
    time.sleep(3)

# function to go to specific FK configuration
def goto_EE(jointVals):
    move_group.go(jointVals, wait=True)
    move_group.stop()
    time.sleep(1)

#######################################
########################################## START OF TASK ##########################################

# We want to initialize to a place other than the default home configuration so we don't run into issues
print('Initializing...')

home_config()    
print('Initialized')
time.sleep(1)

################### INITIAL K ###################
print("Starting First Initial 'K'...")
time.sleep(1)
print('First Stroke...')

# joint values for highest vertical point in K
stroke1a = [-2.269, -1.191, -1.326, -0.669, 1.057, 1.607]
goto_EE(stroke1a)

# joint values for lowest verticak point in K
stroke1b = [-2.156, -1.336, -2.737, 0.884, 0.944, 1.613]
goto_EE(stroke1b)

# go back to home
home_config()

print('Second Stroke...')
# Joint values for the top diagonal
stroke2 = [-1.291, -1.225, -1.520, -0.853, 0.087, 2.039]
goto_EE(stroke2)

# go back to home
home_config()

print('Starting Third Stroke...')
# joint values for bottom-right diagonal stroke
stroke3 = [-1.932, -1.996, -2.271, 1.068, 0.721, 1.628]
goto_EE(stroke3)

# go back to home
home_config()
print("Initial 'K' Done")
time.sleep(3)

################### INITIAL E ###################

print("Starting Second Initial 'E'...")
time.sleep(1)
print('First Stroke...')

# joint values for highest vertical point in K
stroke1a = [-2.269, -1.191, -1.326, -0.669, 1.057, 1.607]
goto_EE(stroke1a)
 
# go outwards for first stroke
stroke1b = [-1.405, -1.897, -0.619, -0.824, 0.196, 1.779]
goto_EE(stroke1b)

# come back to corner position
stroke1a = [-2.269, -1.191, -1.326, -0.669, 1.057, 1.607]
goto_EE(stroke1a)

home_config()

print('Second Stroke...')
# from home, move out directly horziontally for second strke
stroke2 = [-1.369, -1.799, -1.514, -0.068, 0.160, 1.822]
goto_EE(stroke2)

home_config()

print('Third Stroke...')
# come down to lowest K point
stroke3a = [-2.156, -1.336, -2.737, 0.884, 0.944, 1.613]
goto_EE(stroke3a)

# move out to make the last stroke
stroke3b = [-1.357, -2.005, -1.921, 0.528, 0.149, 1.839]
goto_EE(stroke3b)

#go back to lower corner position
stroke3a = [-2.156, -1.336, -2.737, 0.884, 0.944, 1.613]
goto_EE(stroke3a)

home_config()
print("Initial 'E' Done")
time.sleep(3)

################### INITIAL Y ###################

print("Starting Third Initial 'Y'...")
time.sleep(1)
print('First Stroke...')

# move diagonal (up-left) for first stroke
stroke1 = [-3.437, -1.445, -1.165, -0.580, 2.225, 1.556]
goto_EE(stroke1)

home_config()

print('Second Stroke...')
# move diagonal (up-right) for second stroke
stroke2 = [-1.436, -1.362, -1.205, -0.742, 0.227, 1.749]
goto_EE(stroke2)

home_config()

print('Third Stroke...')
# move down for last stroke in Y
stroke3a = [-2.156, -1.336, -2.737, 0.884, 0.944, 1.613]
goto_EE(stroke3a)

home_config()

print("Initial 'Y' Done")
time.sleep(2)
print("3 Initials, 'K-E-Y' Complete")