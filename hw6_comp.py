# Kent Yamamoto
# ME 555.11 - HW6 (Computational)
# Drawing first initial using EE of UR5 robot

# import all packages required #
import sys
import copy
import rospy
import time
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from math import pi
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

# initialize Moveit and start communication with ROS
moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('move_group_python_interface_tutorial',
                anonymous=True)

# initialize robot and scene
robot = moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()

# specify that I'm referring to the manipulator position and orientation from the uR5 URDF file
group_name = "manipulator"
move_group = moveit_commander.MoveGroupCommander(group_name)


## START OF ACTION ###
# We want to initialize to a place other than the default home configuration b/c of bugs
print('Initializing...')

# initialize joint_gla variable by collecting starting position
joint_goal = move_group.get_current_joint_values()
# specify joint_goal values for starting "home" position
joint_goal[0] = -2.211
joint_goal[1] = -0.955
joint_goal[2] = -2.211
joint_goal[3] = -0.021
joint_goal[4] = 0.999
joint_goal[5] = 1.610

# FK for position and orientation
move_group.go(joint_goal, wait=True)
move_group.stop()

print('Initialized')

# wait 1 second for timing
time.sleep(1)

print('Starting First Initial...')

print('First Stroke...')

# joint values for highest vertical point in K
joint_goal[0] = -2.269
joint_goal[1] = -1.191
joint_goal[2] = -1.326
joint_goal[3] = -0.669
joint_goal[4] = 1.057
joint_goal[5] = 1.607
move_group.go(joint_goal, wait=True)
move_group.stop()

# Pause for 1 second
time.sleep(1)

# joint values for lowest verticak point in K
joint_goal[0] = -2.174
joint_goal[1] = -1.090
joint_goal[2] = -2.597
joint_goal[3] = 0.499
joint_goal[4] = 0.962
joint_goal[5] = 1.612
move_group.go(joint_goal, wait=True)
move_group.stop()

time.sleep(1)

# come back to middle of the first stroke
### STARTING POINT ###
joint_goal[0] = -2.211
joint_goal[1] = -0.955
joint_goal[2] = -2.211
joint_goal[3] = -0.021
joint_goal[4] = 0.999
joint_goal[5] = 1.610
move_group.go(joint_goal, wait=True)
move_group.stop()

time.sleep(3)
### STARTING POINT ###

print('Starting Second Stroke...')
# Joint values for the top diagonal
joint_goal[0] = -1.291
joint_goal[1] = -1.225
joint_goal[2] = -1.520
joint_goal[3] = -0.853
joint_goal[4] = 0.087
joint_goal[5] = 2.039
move_group.go(joint_goal, wait=True)
move_group.stop()

time.sleep(1)

# return to starting position to complete the top-right diagonal stroke
### STARTING POINT ###
joint_goal[0] = -2.211
joint_goal[1] = -0.955
joint_goal[2] = -2.211
joint_goal[3] = -0.021
joint_goal[4] = 0.999
joint_goal[5] = 1.610
move_group.go(joint_goal, wait=True)
move_group.stop()

time.sleep(3)
### STARTING POINT ###

print('Starting Third Stroke...')
# joint values for bottom-right diagonal stroke
joint_goal[0] = -1.932
joint_goal[1] = -1.996
joint_goal[2] = -2.271
joint_goal[3] = 1.068
joint_goal[4] = 0.721
joint_goal[5] = 1.628
move_group.go(joint_goal, wait=True)
move_group.stop()

print("Initial 'K' Done")